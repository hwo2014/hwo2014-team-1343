#ifndef _PIECES_HPP_H__
#define _PIECES_HPP_H__

class Pieces
{
    public:
        struct Piece
        {
            int mLength;
            bool mSwitch;
            int mAngle;
            int mRadius;
        };

        Pieces();
       ~Pieces();

        void PushBack(int, bool, int, int);
        int  Size() const;

    private:
        std::vector <Piece> m_List;
};

class Lanes
{
    public:
        Lanes();
       ~Lanes();

        void PushBack(int, int);
        int  Size() const;

    private:
        std::vector< int > m_Index;
        std::vector< int > m_Distance; // From Center
};

class Actions
{
    public:
        struct Action
        {

        };

        Actions();
       ~Actions();
        
        void Init(int);

    private:
        Action* m_Actions;
        int     m_Size;
};

#endif //_PIECES_HPP_H__