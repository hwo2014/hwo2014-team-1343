Pieces::Pieces() { }

Pieces::~Pieces() { }

void Pieces::PushBack(int aLength, bool aSwitch, int aAngle, int aRadius)
{
    Piece p;

    p.mLength = aLength;
    p.mSwitch = aSwitch;
    p.mAngle  = aAngle;
    p.mRadius = aRadius;

    m_List.push_back(p);
}

int Pieces::Size() const
{
    return m_List.size();
}

Lanes::Lanes() { }

Lanes::~Lanes() { }

void Lanes::PushBack(int aIndex, int aDistance)
{
    m_Index.push_back(aIndex);
    m_Distance.push_back(aDistance);
}

int Lanes::Size() const
{
    return m_Index.size();
}

Actions::Actions() 
{
    m_Actions = NULL;
}

Actions::~Actions() 
{
    if (m_Actions) delete[] m_Actions;
}

void Actions::Init(int aCount)
{
    if (m_Actions) delete[] m_Actions;

    if (aCount > 0)
    {
        m_Actions = new Action[aCount];
        m_Size = aCount;
    }
    else
    {
        m_Actions = NULL;
        m_Size = 0;
    }
}