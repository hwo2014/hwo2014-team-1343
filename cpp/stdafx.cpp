#include <Windows.h>
#include <stdio.h>
#include "stdafx.h"

void debug_color(int color, const char* fmt, ...)
{
    char str[DEBUG_OUT_BUFFER_SIZE];

    va_list arg_list;

    va_start(arg_list, fmt);
    vsnprintf(str, DEBUG_OUT_BUFFER_SIZE - 1, fmt, arg_list);

    if(str[0] == 0 || str[strlen(str)-1] != '\n')
    {
        strcat(str, "\n");
    }

    OutputDebugStringA(str);

    HANDLE console;
    int defaultAttributes;

    if(color != 0)
    {
        console = GetStdHandle(STD_OUTPUT_HANDLE);
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(console,&csbi);
        defaultAttributes = csbi.wAttributes;
    }

    switch(color)
    {
    case 1: //red
        SetConsoleTextAttribute(console, FOREGROUND_RED | FOREGROUND_INTENSITY);
        break;

    case 2: // yellow
        SetConsoleTextAttribute(console, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
        break;

    case 3: // green
        SetConsoleTextAttribute(console, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
        break;

    case 4: // blue
        SetConsoleTextAttribute(console, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
        break;
    }

    printf("%s", str);

    if(color != 0)
    {
        SetConsoleTextAttribute(console, defaultAttributes);
    }
};