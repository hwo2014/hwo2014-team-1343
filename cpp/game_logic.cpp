#include "stdafx.h"
#include "protocol.h"
#include <thread>
#include "hpp.h"
#include "game_logic.h"
#include "cpp.h"

using namespace hwo_protocol;

game_logic::game_logic()
#ifndef WIN32
    : action_map
        {
            { "join", &game_logic::on_join },
            { "gameStart", &game_logic::on_game_start },
            { "carPositions", &game_logic::on_car_positions },
            { "crash", &game_logic::on_crash },
            { "gameEnd", &game_logic::on_game_end },
            { "error", &game_logic::on_error },
            { "yourCar", &game_logic::on_your_car },
            { "gameInit", &game_logic::on_game_init },
            { "lapFinished", &game_logic::on_lap_finished },
            { "turboAvailable", &game_logic::on_turbo_available },
            { "tournamentEnd", &game_logic::on_tournament_end },
            { "spawn", &game_logic::on_spawn }
        }
#endif
{
#ifdef WIN32
    action_map["join"] = std::bind(&game_logic::on_join, this, std::placeholders::_1);
    action_map["gameStart"] = std::bind(&game_logic::on_game_start, this, std::placeholders::_1);
    action_map["carPositions"] = std::bind(&game_logic::on_car_positions, this, std::placeholders::_1);
    action_map["crash"] = std::bind(&game_logic::on_crash, this, std::placeholders::_1);
    action_map["gameEnd"] = std::bind(&game_logic::on_game_end, this, std::placeholders::_1);
    action_map["error"] = std::bind(&game_logic::on_error, this, std::placeholders::_1);
    action_map["yourCar"] = std::bind(&game_logic::on_your_car, this, std::placeholders::_1);
    action_map["gameInit"] = std::bind(&game_logic::on_game_init, this, std::placeholders::_1);
    action_map["lapFinished"] = std::bind(&game_logic::on_lap_finished, this, std::placeholders::_1);
    action_map["turboAvailable"] = std::bind(&game_logic::on_turbo_available, this, std::placeholders::_1);
    action_map["tournamentEnd"] = std::bind(&game_logic::on_tournament_end, this, std::placeholders::_1);
    action_map["spawn"] = std::bind(&game_logic::on_spawn, this, std::placeholders::_1);
#endif
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
    const auto& msg_type = msg["msgType"].as<std::string>();
    const auto& data = msg["data"];
    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end())
    {
    #ifdef WIN32
        return (action_it->second)(data);
    #else
        return (action_it->second)(this, data);
    #endif
    }
    else
    {
        LOG_R("Unknown message type: %s\n", msg_type.c_str());
        CALL_RETURN(return, make_ping());
    }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
    LOG("Joined: %s\n", data.to_string().c_str());
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
    LOG_G("Race started.\n");
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
    CALL_RETURN(return, make_throttle(0.6));
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
    LOG_R("Someone crashed: %s\n", data.to_string().c_str());
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
    LOG_G("Race ended: %s\n", data.to_string().c_str());
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
    LOG_R("Error: %s\n", data.to_string().c_str());
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
    LOG("My car: %s\n", data.to_string().c_str());
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
    onGetData(data);
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
    LOG("Tournament Ended.\n");
    CALL_RETURN(return, make_ping());
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
    CALL_RETURN(return, make_ping());
}

void game_logic::onGetData(const jsoncons::json& aData)
{
    jsoncons::json *data = new jsoncons::json(aData);
    new std::thread(std::bind(&game_logic::onProcessData, this, data));
}

void game_logic::onProcessData(jsoncons::json *aData)
{
    {
        GetTime("Load Init Data");

        for (auto it = (*aData)["race"]["track"]["pieces"].begin_elements(), ed = (*aData)["race"]["track"]["pieces"].end_elements(); it != ed; ++it)
        {
            int length = -1;
            bool swchlane = false;
            int angle = -1;
            int radius = -1;

            if ((*it).has_member("length"))
                length = (*it)["length"].as_int();

            if ((*it).has_member("switch"))
                swchlane = (*it)["switch"].as_bool();

            if ((*it).has_member("radius"))
                radius = (*it)["radius"].as_int();

            if ((*it).has_member("angle"))
                angle = (*it)["angle"].as_int();

            m_Pieces.PushBack(length, swchlane, angle, radius);
        }

        for (auto it = (*aData)["race"]["track"]["lanes"].begin_elements(), ed = (*aData)["race"]["track"]["lanes"].end_elements(); it != ed; ++it)
        {
            m_Lanes.PushBack((*it)["index"].as_int(), (*it)["distanceFromCenter"].as_int());
        }

        m_Laps = (*aData)["race"]["raceSession"]["laps"].as_int();
    }

    LOG_B("Piece Total: %d\n", m_Pieces.Size());
    LOG_B("Lane Total: %d\n", m_Lanes.Size());
    LOG_B("Lap Total: %d\n", m_Laps);

    Calculate();

    delete aData;
}

void game_logic::Calculate()
{
    GetTime("Calculate data");

    m_Actions.Init(m_Pieces.Size());
}